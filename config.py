from os import environ, path

basedir = path.abspath(path.dirname(__file__))


class Config:
    SECRET_KEY = 'f3cfe9ed8fae309f02079dbf'
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://postgres:postgres@xo-db:5432/xo'

class ProdConfig(Config):
    DEBUG = False
