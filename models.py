from exts import app, db
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)

class User(db.Model):
    """
    Class for user account
    """
    __tablename__ = 'users'
    id = db.Column((db.Integer), primary_key=True)
    username = db.Column(db.String(120), index=True)
    password_hash = db.Column(db.String(120))
    games = db.relationship('Game', backref='users')


    def __init__(self, username):
        self.username = username

    def serialize(self):
        return {
            "id": self.id,
            "username": self.username,
            "password": self.password,
        }

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration = 3600):
        s = Serializer(app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = User.query.get(data['id'])
        return user

    def __repr__(self):
        return f"<User {self.username}>"

class Game(db.Model):
    """
    Class for storing games
    """
    __tablename__='games'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250), nullable=False)
    description = db.Column(db.String(500), nullable=False)
    board = db.Column(db.JSON, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user_symbol = db.Column(db.String(1), index=False)


class Token(db.Model):
    """
    Class for users authentication tokens
    """
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    value = db.Column(db.String(120))

    def __init__(self, user_id):
        self.user_id = user_id
        self.value = str(uuid4())

    def serialize(self):
        return {
            "id": self.id,
            "user_id": self.user_id,
            "value": self.value,
        }
